function create_wolf_card(link_lobo, nome, idade, descricao, adotado){

    let image1 = document.createElement("img")
    let nome1 = document.createElement("h2")
    let idade1 = document.createElement("p")
    let descricao1 = document.createElement("p")
    let adopted = document.createElement("h1")

    let lobo_p = document.createElement("p")
    let link1 = document.createElement("a")

    nome1.innerText = nome
    idade1.innerText = idade
    descricao1.innerText = descricao
    adopted.innerText = adotado

    link1.setAttribute("href", link_lobo)
    link1.innerText = link_lobo
    lobo_p.append(link1)

    image1.setAttribute("src", link_lobo)

    let wolf_section = document.querySelector("#lobo1")

    wolf_section.append(image1)
    wolf_section.append(nome1)
    wolf_section.append(idade1)
    wolf_section.append(lobo_p)
    wolf_section.append(descricao1)
    wolf_section.append(adopted)
}


async function getWolf(){

    const fetchConfig = {
        "method": "GET"
    }

    await fetch("lobinhos.json", fetchConfig)
        .then((document)=>{
            document.json()
                .then((message)=>{
                    message.forEach(wolf => {
                        create_wolf_card(wolf.imagem, wolf.nome, wolf.idade, wolf.descricao, wolf.adotado)
                    });
                })
                .catch((err)=>{
                    console.log(err)
                })
        })
        .catch((err)=>{
            console.log(err)
        })

}


getWolf()







